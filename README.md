## Introduction

This example demonstrates hot to use GitVersion with .NET Core project and GitLab CI/CD pipelines to add semver versioning to your product and images.


### Version rules

#### Develop (alphas)

Develop branch has image tags like `0.1.0-alpha.16` where 16 is the number of commits since last label `0.1.0`.
Full informational version also contains branch name and commits' SHA: `0.1.0-alpha.16+Branch.develop.Sha.b9492d465b0f02dce2f4c90c3ed9a7e988ca866d`.
Images from this branch are tagged as `alpha`.

#### Release/xxx (betas)

Relase branches regarding gitflow should have name in format `release/0.2.0`, where `0.2.0` is the number of future version. Such branches will nave suffix `beta`, example:
image will have tag `0.2.0-beta.1+3` and full informational version will be: `0.2.0-beta.1+3.Branch.release-0.2.0.Sha.c56f3216fdc536312aab4cea8485ff181c4a1ae3`.
Images from this branch are tagged as `beta`.

#### Master (releases)

After merging release branch back into master, version will bump up and become `0.2.0`
Images from this branch are tagged as `stable`.


## Materials to read    

https://gitversion.net/docs/reference/version-increments

https://www.continuousimprover.com/2022/02/gitversion.html